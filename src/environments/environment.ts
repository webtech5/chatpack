// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	firebase: {
		apiKey: 'AIzaSyBp8yWuKXY_-BV4vxf9yZD2ixs7HRGI80U',
		authDomain: 'chatpack-webtech.firebaseapp.com',
		projectId: 'chatpack-webtech',
		storageBucket: 'chatpack-webtech.appspot.com',
		messagingSenderId: '239907761334',
		appId: '1:239907761334:web:43cc90447e33262e9e0a17',
		measurementId: 'G-LX2SFZZ4M6',
	},
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
