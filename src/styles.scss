// Custom Theming for Angular Material
// For more information: https://material.angular.io/guide/theming
@import "~@angular/material/theming";
// Plus imports for other components in your app.
// Include the common styles for Angular Material. We include this here so that you only
// have to load a single css file for Angular Material in your app.
// Be sure that you only ever include this mixin once!
@include mat-core();

// Define the palettes for your theme using the Material Design palettes available in palette.scss
// (imported above). For each palette, you can optionally specify a default, lighter, and darker
// hue. Available color palettes: https://material.io/design/color/
$chatpack-primary: mat-palette($mat-blue);
$chatpack-accent: mat-palette($mat-indigo);
$chatpack-background: mat-palette($mat-grey);
// The warn palette is optional (defaults to red).
$chatpack-warn: mat-palette($mat-red);

// Create the theme object. A theme consists of configurations for individual
// theming systems such as "color" or "typography".
$chatpack-theme: mat-dark-theme(
	(
		color: (
			primary: $chatpack-primary,
			accent: $chatpack-accent,
			warn: $chatpack-warn,
			background: $chatpack-background,
		),
	)
);

// Include theme styles for core and each component used in your app.
// Alternatively, you can import and @include the theme mixins for each component
// that you are using.
@include angular-material-theme($chatpack-theme);

/* You can add global styles to this file, and also import other style files */

html,
body {
	height: 100%;
}
body {
	margin: 0;
	font-family: Roboto, "Helvetica Neue", sans-serif;
}

.chatpack-app-background {
	background-color: mat-color($chatpack-background, 800);
	color: white;
}

input:-webkit-autofill,
input:-webkit-autofill:hover,
input:-webkit-autofill:focus,
input:-webkit-autofill:active {
	box-shadow: 0 0 0 30px mat-color($chatpack-background, 800) inset !important;
	-webkit-box-shadow: 0 0 0 30px mat-color($chatpack-background, 800) inset !important;
	-webkit-text-fill-color: white !important;
}
