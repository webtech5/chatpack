import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import firebase from 'firebase';
import { FriendDialogComponent } from './components/friend-dialog/friend-dialog.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/User';
import { AddFriendDialogComponent } from './components/add-friend-dialog/add-friend-dialog.component';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { take } from 'rxjs/operators';
import { FriendsDialogComponent } from './components/friends-dialog/friends-dialog.component';
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
	user$: Observable<User>;
	friendRequests: User[] = new Array();
	subscription = new Subscription();

	constructor(
		public auth: AngularFireAuth,
		private router: Router,
		public dialog: MatDialog,
		private afs: AngularFirestore,
	) {}

	ngOnInit(): void {
		this.subscription.add(
			this.auth.authState.subscribe((authUser) => {
				this.user$ = this.afs.doc<User>('users/' + authUser.uid).valueChanges();

				this.subscription.add(
					this.user$.subscribe((user) => {
						console.log(user);
						if (!user.friends || !user.friendRequests) {
							user.friends = new Array();
							user.friendRequests = new Array();
							this.afs.doc('users/' + user.uid).update(user);
						}
						this.updateFriendRequests();
					}),
				);
			}),
		);
	}

	logout(): void {
		this.auth.signOut();
		this.router.navigateByUrl('login');
	}

	showAccount(): void {
		this.router.navigateByUrl('login');
	}

	showFriendDialog(user: User): void {
		const dialogRef = this.dialog.open(FriendDialogComponent, {
			width: '250px',
			data: user,
		});
	}

	showFriendsList(): void {
		const dialogRef = this.dialog.open(FriendsDialogComponent, {
			width: '300px',
		});
	}

	showInfo(): void {
		const dialogRef = this.dialog.open(InfoDialogComponent, {
			width: '300px',
		});
	}

	addFriend(): void {
		const dialogRef = this.dialog.open(AddFriendDialogComponent, {
			width: '300px',
		});
	}

	updateFriendRequests(): void {
		this.subscription.add(
			this.user$.pipe(take(1)).subscribe((user) => {
				this.friendRequests = new Array();
				console.log(user.friendRequests);
				user.friendRequests.forEach((friendReq) => {
					this.afs
						.doc<User>('users/' + friendReq)
						.get()
						.toPromise()
						.then((data) => {
							console.log(data.data());
							this.friendRequests.push(data.data());
						});
				});
			}),
		);
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}
}
