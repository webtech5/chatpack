import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
	AngularFirestore,
	AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Chat } from 'src/app/models/Chat';
import { Observable, Subscription } from 'rxjs';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Message } from 'src/app/models/Message';
import { User } from '../../models/User';
import { FormControl } from '@angular/forms';
import firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { LocalChat } from '../../models/Chat';
import { LocalMessage } from '../../models/Message';
import randomColor from 'randomcolor';

@Component({
	selector: 'app-chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {
	chatId!: string | null;
	chatDoc!: AngularFirestoreDocument<LocalChat>;
	chat$!: Observable<LocalChat>;
	messageCollection: AngularFirestoreCollection<Message>;
	messages$: Observable<Message[]>;
	messages: Message[] = new Array();
	userDoc: AngularFirestoreDocument<User>;
	users$: Observable<User>[] = new Array();
	authUser: firebase.User;

	message = new FormControl();

	colorMap = new Map<string, string>();

	subscription = new Subscription();

	constructor(
		private readonly afs: AngularFirestore,
		private route: ActivatedRoute,
		auth: AngularFireAuth,
	) {
		auth.authState.subscribe((user) => (this.authUser = user));
		this.route.paramMap
			.subscribe((params) => {
				this.chatId = params.get('id');
			})
			.unsubscribe();
		this.chat$ = afs
			.doc<Chat>('chats/' + this.chatId)
			.valueChanges({ idField: 'id' });
		this.subscription.add(
			afs
				.collection<Message>('chats/' + this.chatId + '/messages', (ref) =>
					ref.orderBy('timeSent'),
				)
				.stateChanges(['added'])
				.subscribe((action) => {
					action.map((a) => {
						const data = a.payload.doc.data() as Message;
						const id = a.payload.doc.id;
						this.setRead({ id, ...data } as LocalMessage);
						this.messages.push(data);
						if (document.getElementById('message-container').scrollTop > -150) {
							document.getElementById('message-container').scrollTop = 0;
						}
					});
				}),
		);
		this.subscription.add(
			this.chat$.subscribe((chat) => {
				chat.users.forEach((user) => {
					this.users$.push(afs.doc<User>('users/' + user).valueChanges());
					this.colorMap.set(
						user,
						randomColor({
							luminosity: 'bright',
							hue: 'blue',
							seed: user,
						}),
					);
				});
			}),
		);
	}

	ngOnInit(): void {}

	setRead(message: LocalMessage): void {
		if (!message.readBy?.includes(this.authUser.uid)) {
			message.readBy.push(this.authUser.uid);
			this.afs
				.doc('chats/' + this.chatId + '/messages/' + message.id)
				.update(message);
		}
	}

	sendMessage(): void {
		if (this.message.value !== '') {
			const message: Message = {
				content: this.message.value,
				fromUser: this.authUser.uid,
				fromUserName: this.authUser.displayName,
				timeSent: firebase.firestore.Timestamp.fromDate(new Date()),
				readBy: [this.authUser.uid],
			};
			this.subscription.add(
				this.chat$.subscribe((chat) => {
					this.afs.collection('chats/' + chat.id + '/messages').add(message);
					this.message.setValue('');
				}),
			);
		}
	}

	addEmoji($event: { emoji: { native: any } }): void {
		this.message.patchValue(
			(this.message.value ? this.message.value : '') + $event.emoji.native,
		);
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}
}
