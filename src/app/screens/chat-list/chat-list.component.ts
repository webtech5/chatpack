import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, of, Subscription } from 'rxjs';
import { Chat } from 'src/app/models/Chat';
import { AngularFireAuth } from '@angular/fire/auth';
import { AddChatDialogComponent } from 'src/app/components/add-chat-dialog/add-chat-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import firebase from 'firebase';
import { LocalChat } from '../../models/Chat';
import { Message } from 'src/app/models/Message';
@Component({
	selector: 'app-chat',
	templateUrl: './chat-list.component.html',
	styleUrls: ['./chat-list.component.scss'],
})
export class ChatListComponent implements OnInit, OnDestroy {
	chats$: Observable<LocalChat[]>;
	authUser: firebase.User;
	subscription = new Subscription();

	constructor(
		public auth: AngularFireAuth,
		private afs: AngularFirestore,
		public dialog: MatDialog,
	) {}

	ngOnInit(): void {
		this.subscription.add(
			this.auth.authState.subscribe((user) => {
				this.authUser = user;
				this.chats$ = this.afs
					.collection<Chat>('chats', (ref) =>
						ref.where('users', 'array-contains', this.authUser.uid),
					)
					.valueChanges({ idField: 'id' });
				this.chats$.subscribe((chats) => {
					chats.forEach((c) => {
						this.afs
							.collection<Message>('chats/' + c.id + '/messages')
							.valueChanges({ idField: 'id' })
							.subscribe((messages) => {
								c.readCount = 0;
								messages.forEach((message) => {
									if (!message.readBy.includes(this.authUser.uid)) {
										c.readCount++;
									}
									c.latestMessage = messages.sort(
										(a, b) => b.timeSent - a.timeSent,
									)[0];
								});
							});
						this.chats$ = of(chats);
					});
				});
			}),
		);
	}

	addChat(): void {
		const dialogRef = this.dialog.open(AddChatDialogComponent, {
			width: '300px',
		});
	}

	editChat(chat: LocalChat): void {
		const dialogRef = this.dialog.open(AddChatDialogComponent, {
			width: '300px',
			data: chat,
		});
	}

	removeChat(chat: LocalChat): void {
		const modifiedChat = {
			name: chat.name,
			users: chat.users.filter((u) => u !== this.authUser.uid),
		} as Chat;
		this.afs.doc<Chat>('chats/' + chat.id).update(modifiedChat);
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}
}
