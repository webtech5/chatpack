import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthProvider } from 'ngx-auth-firebaseui';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
	providers = AuthProvider;

	showLogin = false;
	showSplash = false;

	constructor(private router: Router, private auth: AngularFireAuth) {}

	ngOnInit(): void {
		this.auth.user.subscribe((user) => {
			if (user === null) {
				this.showLogin = true;
				this.showSplash = true;
			} else {
				this.showLogin = false;
			}
		});
	}

	flyAway(): void {
		document.getElementById('splash-screen').classList.add('active');
		setTimeout(() => {
			this.showSplash = false;
		}, 2000);
	}
}
