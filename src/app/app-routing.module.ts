import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './screens/login/login.component';
import { ChatListComponent } from './screens/chat-list/chat-list.component';
import { LoggedInGuard } from 'ngx-auth-firebaseui';
import { ChatComponent } from './screens/chat/chat.component';

const routes: Routes = [
	{ path: '', component: ChatListComponent, canActivate: [LoggedInGuard] },
	{ path: 'login', component: LoginComponent },
	{ path: 'chat/:id', component: ChatComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
