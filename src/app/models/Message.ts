export interface Message {
	content: string;
	fromUser: string;
	fromUserName: string;
	timeSent: any;
	readBy: string[];
}

export interface MessageWithId extends Message {
	id: string;
}

export interface LocalMessage extends MessageWithId {
	read?: boolean;
}
