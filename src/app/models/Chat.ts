import { User } from './User';
import { Message } from './Message';

export interface Chat {
	users: string[];
	userNames: string[];
	name: string;
}

export interface LocalChat extends Chat {
	id: string;
	readCount?: number;
	latestMessage?: Message;
	messages?: Message[];
}
