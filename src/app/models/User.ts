export interface User {
	id: string;
	displayName: string;
	email: string;
	phoneNumber: string;
	photoUrl: string;
	providerId: string;
	uid: string;
	friends: string[];
	friendRequests: string[];
}

export interface LocalUser extends User {
	friendUsers: User[];
}
