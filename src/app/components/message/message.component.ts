import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Message } from '../../models/Message';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/User';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
	selector: 'app-message',
	templateUrl: './message.component.html',
	styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit, OnDestroy {
	@Input() message: Message;
	@Input() color: string;

	fromUser$: Observable<User>;
	fromUserId: string;
	time: string;
	userId: string;
	isFromMe: boolean;
	content: string;

	subscription = new Subscription();

	constructor(
		public auth: AngularFireAuth,
		private readonly afs: AngularFirestore,
	) {}

	ngOnInit(): void {
		this.fromUser$ = this.afs
			.doc<User>('users/' + this.message.fromUser)
			.valueChanges();

		this.subscription.add(
			this.auth.user.subscribe((user) => {
				this.userId = user.uid;
				this.isFromMe = user.uid === this.message.fromUser;
			}),
		);
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}
}
