import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { User } from 'src/app/models/User';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { first } from 'rxjs/operators';
import firebase from 'firebase';
import { ValidatorFn, AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-friend-dialog',
	templateUrl: './friend-dialog.component.html',
	styleUrls: ['./friend-dialog.component.scss'],
})
export class FriendDialogComponent implements OnInit, OnDestroy {
	user: firebase.User;

	subscription = new Subscription();

	constructor(
		public dialogRef: MatDialogRef<FriendDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: User,
		private afs: AngularFirestore,
		public readonly auth: AngularFireAuth,
	) {
		this.subscription.add(
			auth.authState.subscribe((user) => {
				this.user = user;
			}),
		);
	}

	ngOnInit(): void {}

	accept(): void {
		this.afs
			.doc<User>('users/' + this.user.uid)
			.get()
			.toPromise()
			.then((user) => user.data())
			.then((user) => {
				let friendReq = [...user.friendRequests];
				const friendsArr = [...user.friends];
				friendReq = friendReq.filter((req) => req !== this.data.uid);
				friendsArr.push(this.data.uid);
				this.afs.doc<User>('users/' + user.uid).update({
					friends: friendsArr,
					friendRequests: friendReq,
				});
			});
		this.afs
			.doc<User>('users/' + this.data.uid)
			.get()
			.toPromise()
			.then((user) => user.data())
			.then((user) => {
				const friendsArr = [...user.friends];
				friendsArr.push(this.user.uid);
				this.afs.doc<User>('users/' + user.uid).update({
					friends: friendsArr,
				});
			});
		this.dialogRef.close();
	}

	decline(): void {
		this.dialogRef.close();
	}

	remove(): void {
		this.afs
			.doc<User>('users/' + this.user.uid)
			.get()
			.toPromise()
			.then((user) => user.data())
			.then((user) => {
				let friendReq = [...user.friendRequests];
				friendReq = friendReq.filter((req) => req !== this.data.uid);
				this.afs.doc<User>('users/' + user.uid).update({
					friendRequests: friendReq,
				});
			});
		this.dialogRef.close();
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}
}
