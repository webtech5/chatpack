import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/models/User';
import firebase from 'firebase';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
	selector: 'app-add-friend-dialog',
	templateUrl: './friends-dialog.component.html',
	styleUrls: ['./friends-dialog.component.scss'],
})
export class FriendsDialogComponent implements OnInit {
	authUser: firebase.User;
	user: User;
	friends: User[] = new Array();

	subscription = new Subscription();

	constructor(
		public afs: AngularFirestore,
		public readonly auth: AngularFireAuth,
		public dialogRef: MatDialogRef<FriendsDialogComponent>,
	) {}

	ngOnInit(): void {
		this.subscription.add(
			this.auth.authState.subscribe((user) => {
				this.authUser = user;
				this.afs
					.doc<User>('users/' + this.authUser.uid)
					.valueChanges()
					.subscribe((user) => {
						this.user = user;
						this.friends = new Array();
						user.friends.forEach((friend) => {
							console.log(friend);
							this.afs
								.doc<User>('users/' + friend)
								.get()
								.subscribe((data) => {
									this.friends.push(data.data());
								});
						});
					});
			}),
		);
	}

	removeFriend(user: User) {
		this.user.friends = this.user.friends.filter(
			(friend) => friend !== user.uid,
		);
		user.friends = user.friends.filter((friend) => friend !== this.user.uid);
		console.log(this.user, user);
		this.afs.doc<User>('users/' + this.authUser.uid).update(this.user);
		this.afs.doc<User>('users/' + user.uid).update(user);
	}
}
