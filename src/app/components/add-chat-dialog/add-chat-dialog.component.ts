import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/models/User';
import firebase from 'firebase';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MyErrorStateMatcher } from '../add-friend-dialog/add-friend-dialog.component';
import { Chat } from 'src/app/models/Chat';
import { LocalChat } from '../../models/Chat';

@Component({
	selector: 'app-add-chat-dialog',
	templateUrl: './add-chat-dialog.component.html',
	styleUrls: ['./add-chat-dialog.component.scss'],
})
export class AddChatDialogComponent implements OnInit, OnDestroy {
	isEdit: boolean;
	authUser: firebase.User;
	options: User[] = new Array();
	filteredOptions: Observable<User[]>;
	userInput = new FormControl(Validators.required);
	chatName = new FormControl(Validators.required);
	myForm = new FormGroup({
		userInput: this.userInput,
	});
	matcher = new MyErrorStateMatcher();
	currentUser: User;
	users: User[] = new Array();

	subscription = new Subscription();

	constructor(
		public afs: AngularFirestore,
		public readonly auth: AngularFireAuth,
		public dialogRef: MatDialogRef<AddChatDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: LocalChat,
	) {
		this.subscription.add(
			auth.authState.subscribe((user) => {
				this.authUser = user;
			}),
		);
	}

	ngOnInit(): void {
		if (this.data) {
			this.isEdit = true;
			this.chatName.setValue(this.data.name);
			this.data.users.forEach((user) => {
				this.afs
					.doc<User>('users/' + user)
					.get()
					.subscribe((data) => {
						if (data.data().uid !== this.authUser.uid) {
							this.users.push(data.data());
						}
					});
			});
		} else {
			this.chatName.setValue('');
		}
		this.subscription.add(
			this.afs
				.collection<User>('users')
				.valueChanges()
				.subscribe((users) => {
					users.forEach((user) => {
						console.log(user);
						if (
							user.uid !== this.authUser.uid &&
							user.friends?.includes(this.authUser.uid)
						) {
							this.options.push(user);
						}
					});
				}),
		);
		this.filteredOptions = this.userInput.valueChanges.pipe(
			startWith(''),
			map((value) => (typeof value === 'string' ? value : value.email)),
			map((email) => (email ? this._filter(email) : this.options.slice())),
		);
	}

	displayFn(user: User): string {
		return user && user.email ? user.email : '';
	}

	private _filter(email: string): User[] {
		const filterValue = email.toLowerCase();

		return this.options.filter(
			(option) => option.email.toLowerCase().indexOf(filterValue) === 0,
		);
	}

	addUserToList(): void {
		console.log(typeof this.userInput.value);
		if (typeof this.userInput.value === 'function') return;
		this.users.push(this.userInput.value);
		this.options = this.options.filter(
			(option) => option.uid !== this.userInput.value.uid,
		);
		this.userInput.setValue('');
	}

	removeUserFromList(user: User): void {
		this.users = this.users.filter((u) => {
			console.log(u.uid, user.uid);
			return u.uid !== user.uid;
		});
		this.options.push(user);
	}

	addChat(): void {
		const users = this.users.map((user) => user.uid);
		users.push(this.authUser.uid);
		const data = {
			name: this.chatName.value,
			users,
		};
		this.afs.collection('chats/').add(data);
		this.dialogRef.close();
	}

	updateChat(): void {
		const users = this.users.map((user) => user.uid);
		users.push(this.authUser.uid);
		const data = {
			name: this.chatName.value,
			users,
		};
		this.afs.doc<Chat>('chats/' + this.data.id).update(data);
		this.dialogRef.close();
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}
}
