import { Component, OnInit } from '@angular/core';
import {
	AbstractControl,
	FormControl,
	FormGroup,
	FormGroupDirective,
	NgForm,
	ReactiveFormsModule,
	ValidatorFn,
	Validators,
} from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/models/User';
import firebase from 'firebase';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
	selector: 'app-add-friend-dialog',
	templateUrl: './add-friend-dialog.component.html',
	styleUrls: ['./add-friend-dialog.component.scss'],
})
export class AddFriendDialogComponent implements OnInit {
	authUser: firebase.User;
	options: User[] = new Array();
	filteredOptions: Observable<User[]>;
	myControl = new FormControl(Validators.required);
	myForm = new FormGroup({
		myControl: this.myControl,
	});

	matcher = new MyErrorStateMatcher();

	subscription = new Subscription();

	constructor(
		public afs: AngularFirestore,
		public readonly auth: AngularFireAuth,
		public dialogRef: MatDialogRef<AddFriendDialogComponent>,
	) {
		this.subscription.add(
			auth.authState.subscribe((user) => {
				this.authUser = user;
			}),
		);
	}

	ngOnInit(): void {
		this.subscription.add(
			this.afs
				.collection<User>('users')
				.valueChanges()
				.subscribe((users) => {
					this.options = new Array();
					users.forEach((user) => {
						if (
							user.uid !== this.authUser.uid &&
							!user.friends?.includes(this.authUser.uid)
						) {
							this.options.push(user);
						}
					});
				}),
		);
		this.filteredOptions = this.myControl.valueChanges.pipe(
			startWith(''),
			map((value) => (typeof value === 'string' ? value : value.email)),
			map((email) => (email ? this._filter(email) : this.options.slice())),
		);
	}

	displayFn(user: User): string {
		return user && user.email ? user.email : '';
	}

	private _filter(email: string): User[] {
		const filterValue = email.toLowerCase();

		return this.options.filter(
			(option) => option.email.toLowerCase().indexOf(filterValue) === 0,
		);
	}

	addFriend(): void {
		const friendRequestArray: string[] = this.myControl.value.friendRequests;
		if (!friendRequestArray.includes(this.authUser.uid)) {
			friendRequestArray.push(this.authUser.uid);
		}
		const data = {
			friendRequests: friendRequestArray,
		};
		this.afs.doc('users/' + this.myControl.value.uid).update(data);
		this.dialogRef.close();
	}
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null): boolean {
		const isString = typeof control.value === 'string';
		return !!((control && control.invalid) || isString);
	}
}
