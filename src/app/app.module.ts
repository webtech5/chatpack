import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './screens/login/login.component';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ChatListComponent } from './screens/chat-list/chat-list.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ChatComponent } from './screens/chat/chat.component';
import { MessageComponent } from './components/message/message.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatBadgeModule } from '@angular/material/badge';
import { FriendDialogComponent } from './components/friend-dialog/friend-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AddFriendDialogComponent } from './components/add-friend-dialog/add-friend-dialog.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddChatDialogComponent } from './components/add-chat-dialog/add-chat-dialog.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MarkdownModule } from 'ngx-markdown';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { FriendsDialogComponent } from './components/friends-dialog/friends-dialog.component';
@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		ChatListComponent,
		ChatComponent,
		MessageComponent,
		FriendDialogComponent,
		AddFriendDialogComponent,
		AddChatDialogComponent,
		InfoDialogComponent,
		FriendsDialogComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		AngularFireModule.initializeApp(environment.firebase),
		AngularFirestoreModule,
		BrowserAnimationsModule,
		MatPasswordStrengthModule,
		NgxAuthFirebaseUIModule.forRoot(environment.firebase, () => 'chatpack', {
			enableFirestoreSync: true,
			toastMessageOnAuthError: true,
			authGuardLoggedInURL: '/',
			authGuardFallbackURL: 'login',
			passwordMaxLength: 60,
			passwordMinLength: 6,
			nameMaxLength: 60,
			nameMinLength: 2,
			guardProtectedRoutesUntilEmailIsVerified: true,
			enableEmailVerification: true,
		}),
		FlexLayoutModule,
		MatToolbarModule,
		MatMenuModule,
		MatButtonModule,
		MatIconModule,
		MatCardModule,
		MatListModule,
		MatProgressSpinnerModule,
		MatChipsModule,
		MatFormFieldModule,
		MatInputModule,
		MatBadgeModule,
		MatDialogModule,
		MatAutocompleteModule,
		ReactiveFormsModule,
		FormsModule,
		MatExpansionModule,
		MarkdownModule.forRoot(),
		PickerModule,
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
