# [Chatpack](https://chatpack-webtech.web.app/)

## Description

Chatpack - The most innovative chat app of the year (for now), even more innovative than the current covid measures.

Send and recieve messages in real time, add emojis to your message, format them using complete markdown support (including programming languages!), add friends and annoy them with thousands of group chats, and way more!

And the best part: This was not made by facebook, so you don't even have to worry about Zuckerberg seeing your messages. The only one who sees them are you (and us, but we won't look at them, we promise 😏).

## Technology

- Angular
- [Angular Material](https://material.angular.io/)
- [Firebase](https://firebase.google.com/)
  - Firestore
  - Fireauth

## Dependencies

- Emoji support: [`"@ctrl/ngx-emoji-mart": "^4.1.0"`](https://www.npmjs.com/package/@ctrl/ngx-emoji-mart)
- Literally everything: [`"firebase": "^7.0 || ^8.0"`](https://www.npmjs.com/package/firebase) and [`"@angular/fire": "^6.1.4"`](https://www.npmjs.com/package/@angular/fire)
- Login component: [`"ngx-auth-firebaseui": "^5.0.1"`](https://www.npmjs.com/package/ngx-auth-firebaseui)
- Message formatting: [`"ngx-markdown": "^11.0.1"`](https://www.npmjs.com/package/ngx-markdown)

## Authors

- Florian Dueller

  - Firebase

- Simone Nardone

  - Chat functionality

- Yevhen Andrianov

  - Message functionality

The rest was done by all of us together. Team work!!
